import React, {Component, PropTypes} from 'react';
import classNames from 'classnames';
import styles from './style.css';

class Arrows extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    let self = this;

    return (
      <div className={classNames(this.props.className, styles.arrows_container)}>
        <div className={styles.arrows}>
          <i className={styles.arrows_left}></i>
          <i className={styles.arrows_right}></i>
        </div>
          {this.props.insert}
      </div>
    )
}
}

Arrows.propTypes = {
  insert: PropTypes.element
};

export default Arrows;
