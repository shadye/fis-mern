import React, {Component, PropTypes} from 'react';
import classNames from 'classnames';
import Arrows from '../Arrows';
import styles from './style.css';

class LocalizeSelect extends Component {
	constructor(props, context) {
		super(props, context);
	}

	updateLanguage(e) {
		e.preventDefault();
		//e.target.value
	}

	render() {
		const select = <select className={styles.localize_select} defaultValue="ru" onChange={this.updateLanguage}>
        <option value="ru">Русский</option>
        <option value="en">English</option>
      </select>
		return (
			<div className={ classNames( this.props.className, styles.localize) }>
				<div className={styles.localize_container}>
		          <Arrows className={styles.localize_arrows} insert={select}/>
		        </div>
			</div>
		)
	}
}

export default LocalizeSelect;