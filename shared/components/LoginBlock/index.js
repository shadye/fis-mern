import React, {Component, PropTypes} from 'react';
import classNames from 'classnames';
import styles from './style.css';

class LoginBlock extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			name: '',
			password: ''
		}
		this.handleInput = this.handleInput.bind(this);
		this.login = this.login.bind(this);
	}

	handleInput(e) {
		let temp = {}; 
	    temp[e.target.name] = e.target.value;
	    this.setState(temp);

	    e.preventDefault();
	}

	login(e) {
		e.preventDefault();	
	}

	render() {
		return (
			<div className={classNames(this.props.className, styles.login)}>
		        <div className={styles.login.container}>
		          <form role="form">
		            <input type="text" name="user" onChange={this.handleInput} className={styles.login__input, styles.login__input_user} placeholder=""/>
		            <input type="password" name="password" onChange={this.handleInput} className={styles.login__input, styles.login__input_pass} placeholder=""/>
		            <button className={styles.login__input, styles.login__input_submit} type="submit" onClick={this.login}>></button>
		          </form>
		          <div className={styles.login__helpers}>
		            <a className={styles.login__helpers_forgot}>forgot</a>
		            <a className={styles.login__helpers_registration}>register</a>
		          </div>
		        </div>
	      	</div>
		)
	}
}



export default LoginBlock;