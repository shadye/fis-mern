import { Route, IndexRoute } from 'react-router';
import React from 'react';
import App from './container/App';
import Main from './container/Main';

import PostContainer from './container/PostContainer/PostContainer';
import PostDetailView from './container/PostDetailView/PostDetailView';

const routes = (
  <Route path="/" component={App} >
    <IndexRoute component={Main} />
    <Route path="/post/:slug" component={PostDetailView}/>
  </Route>
);

export default routes;
