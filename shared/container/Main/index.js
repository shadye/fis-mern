import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../../redux/actions/actions';
import styles from './style.css';
import classnames from 'classnames';
import LocalizeSelect from '../../components/LocalizeSelect';
import LoginBlock from '../../components/LoginBlock';

class Main extends Component {
  constructor(props, context) {
    super(props, context);
  }

  handleClick(e) {
    let temp = {}; 
    temp[e.target.name] = e.target.value;
    this.setState(temp);

    e.preventDefault();
  }

  render() {
    return (
      <div className={styles.fis}>
        <div className={styles.header}>
          <span className={styles.header__logo}>fis</span>
          <span className={styles.header__desc}>Первая международная интернет-школа</span>
          <div className={styles.header__socialContainer}>
            <span className={styles.header__socialContainer_desc}>Мы в соц. сетях:</span>
            <div className={styles.header__socialContainer_links}>
              <span className={classnames("fa-stack", styles.fa_stack)}>
                <i className={classnames("fa fa-circle fa-stack-2x", styles.header__socialContainer_linkCircle)}></i>
                <i className={classnames("fa fa-facebook fa-stack-1x", styles.header__socialContainer_link)}></i>
              </span>
              <span className={classnames("fa-stack", styles.fa_stack)}>
                <i className={classnames("fa fa-circle fa-stack-2x", styles.header__socialContainer_linkCircle)}></i>
                <i className={classnames("fa fa-vk fa-stack-1x", styles.header__socialContainer_link)}></i>
              </span>
              <span className={classnames("fa-stack", styles.fa_stack)}>
                <i className={classnames("fa fa-circle fa-stack-2x", styles.header__socialContainer_linkCircle)}></i>
                <i className={classnames("fa fa-youtube fa-stack-1x", styles.header__socialContainer_link)}></i>
              </span>
            </div>
          </div>
          <LocalizeSelect className={styles.header__localize}/>
          <LoginBlock className={styles.header__login}/>
        </div>
      </div>
    );
  }
}

Main.contextTypes = {
  router: React.PropTypes.object
};

function mapStateToProps(store) {
  return {
    posts: store.posts,
  };
}

Main.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
  })).isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default connect(mapStateToProps)(Main);
